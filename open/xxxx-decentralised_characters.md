- Feature Name: Decentralised Characters
- Start Date: 2020-12-01
- RFC Number:
- Tracking Issue:

# Summary
[summary]: #summary

Characters and all of their progress would be decoupled from the server. The player would create and have full personal ownership over their characters, independently from any hosted instance of a game server.

Aspects of a character which do not have direct impact on gameplay and is not gated behind some form of achievement would be unproblematic[1] for any server to allow onto it, such as standard cosmetic options/items, character appearance, name[2], background bio entry, friends lists etc.

Aspects that are products of gameplay achievements would require verification of their validity or a system of trust[3], such as experience/level/stats, skills, equipment and inventory, achievements etc.

1. Unproblematic on the assumption that any user-created content is in compliance with CoC and that the server is feature-compatible. Solutions to allow divergent feature sets between servers will be addressed later in this document.
2. Character names could have a UUID component to allow several characters with the same nickname.
3. Ideas for verifiable and/or trusted achievementsare are discussed in the section "Reference-level explanation".

# Motivation
[motivation]: #motivation

Many people grow attached to their character(s). Some even come to live a second life through them. Players often invest significant effort and time into growing their powers, building a reputation and uniquely defining them by collecting a history of achievements and acquiring personal items.

Players should be the owners of their characters and all earnings from the effort they put in. A player should feel safe in knowing that their achievements and progression will follow wherever they go, and that even if a server becomes inaccessible or if they decide to change servers, their character(s) and progress is not lost forever.

There are countless examples of the sort of tragedies were players invested enourmous time and effort into their character(s) over many years, only to have them permanently disappear when the game service shuts down. Decentralised characters would instill the player with a sense of persistence, continuity and personal ownership, allowing them to invest in their character(s) without the fear of loss or sense of dependency on some remote authority.

To my knowledge, there is no MMORPG that has attempted to solve this problem to a satisfactory degree. Veloren is a perfect opportunity to raise the bar and introduce a new standard for online role-playing games.

# Guide-level explanation
[guide-level-explanation]: #guide-level-explanation

When you launch the game, you are greeted by a unification of the server selection and character selection (which contains a special "new" entry which allows the player to enter initial values to create a new one).

The server list is populated by the official server plus all servers which have at some point attributed something to any of your characters, with the most recent one (or the official server if none) selected by default. A server browser would be available to find a new server to play on.

The character selection list is populated by all characters uniquely attributed to you (personal identification and authentication methods requires further investigation), plus the entry for creating a new one. When the "new" entry is selected, the non-existent character could be represented by whatever placeholder/substitute the server has chosen.

Character creation is part of a more general system: Character Capabilities. This system is designed to solve the problem of different servers supporting or extending some sub- or superset of various character attributes/properties/traits/aspects - i.e. "capabilities". If a server supports a superset of a character's existing capabilities, the server may prompt the player to make initial choices, e.g. starting equipment, class/stats/abilities, items, appearance etc.

How to best present server capabilities and relationships between servers in terms of commonly supported and disjoint capabilities is TBD.

# Reference-level explanation
[reference-level-explanation]: #reference-level-explanation

Characters would have to be modular and extensible in their capabilities. Any given server should specify what a character is, can have and can do.

Given the set of all the character's capabilities A ever attributed to it and the set of capabilities B supported by the server on which it is about to play on:
A ∩ B would be the capabilities used and modified by the server during gameplay. A \ B would be ignored and untouched by the server. B \ A would be newly attributed to the character and subsequently modified during gameplay.

When a player connects and plays with a character, modifications to the values of the character's capabilities would be atomic and signed so that other servers can identify the source of the modifications and be given the chance to reject any with untrusted signatures. This leads to the matter of forks in progression.

Consider the scenario where a player gains a level on server A and switches over to server B, which is mutually trusting with A, and gains another level there plus a special item. They then go to play on server C, which trust A but not B, thus rejecting that last level and item acquired. At this point, the player should be allowed to say "OK whatever, just continue off from where I was on server A" without necessarily losing the experience and item gained on B in case they wish to go back there (or to a server which *does* trust B) later.

To support this system there must be some interface through which to switch between forks in a character's progression, and possibly some form of merging (e.g. server D trusts all of A, B and C and would happily accept all items and experience acquired on all of those, even if they were awarded through divergent progression paths).

Merging may be tricky and would require some logic for progression dependencies in order to resolve conflicts, but even if forks are strictly mutually exclusive, they could be designed to be capability-specific rather than complete forks in a character's general progression. Things like changes in appearance customisation does not necessarily have to be forked just because two server's disagree on what the levelling curve should be. Trust could be on a capability-specific basis.

# Drawbacks
[drawbacks]: #drawbacks

Why should we *not* do this?

This section is *really* important. An RFC is more likely to be accepted if it recognises its own weaknesses and attempts to consider them as part of the proposal.

Remember: Listing drawbacks doesn't mean an RFC won't be accepted. If anything, it gives developers the confidence to move forward with the RFC, knowing what impact it may have.

# Rationale and alternatives
[alternatives]: #alternatives

- Why is this design the best in the space of possible designs?
- What other designs have been considered and what is the rationale for not choosing them?
- What is the impact of not doing this?

# Prior art
[prior-art]: #prior-art

Discuss prior art, both the good and the bad, in relation to this proposal.
A few examples of what this can include are:

- Does this feature exist in other games or engines and what experience have their community had?
- For community proposals: Is this done by some other community and what were their experiences with it?
- For other teams: What lessons can we learn from what other communities have done here?
- Papers: Are there any published papers or great posts that discuss this? If you have some relevant papers to refer to, this can serve as a more detailed theoretical background.

This section is intended to encourage you as an author to think about the lessons from other games: provide readers of your RFC with a fuller picture.
If there is no prior art, that is fine - your ideas are interesting to us whether they are brand new or if it is an adaptation from other projects.

Note that while precedent set by other games is some motivation, it does not on its own motivate an RFC.
Please also take into consideration that Veloren sometimes intentionally diverges from common game features.

# Unresolved questions
[unresolved]: #unresolved-questions

- What parts of the design do you expect to resolve through the RFC process before this gets merged?
- What parts of the design do you expect to resolve through the implementation of this feature before stabilization?
- What related issues do you consider out of scope for this RFC that could be addressed in the future independently of the solution that comes out of this RFC?
